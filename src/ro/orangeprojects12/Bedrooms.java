package ro.orangeprojects12;

public class Bedrooms {

    public static void main(String[] args) {
        Single_Beds b1 = new Single_Beds("brown", "Single Bed", 300, 100, 52);
	    Matrimonial_Beds b2 = new Matrimonial_Beds("brown","Matrimonial Bed",400,250,55,
                "double bed","Scandinavian Pine");

        System.out.println(b1.CalculateSize());
        System.out.println("The color of Single Bed " + b1.getColor());
        System.out.println(b2.CalculateSize());
        System.out.println("Matrimonial Bed is made of " + b2.getMadeOf());

    }
}
