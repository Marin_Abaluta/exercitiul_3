package ro.orangeprojects12;

public class Matrimonial_Beds extends Single_Beds {

    private String style;
    private String madeOf;

    public Matrimonial_Beds(String color, String type, int dimension, int height, int weight, String style, String madeOf) {
        super(color, type, dimension, height, weight);
        this.style = style;
        this.madeOf = madeOf;
    }



    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getMadeOf() {
        return madeOf;
    }

    public void setMadeOf(String madeOf) {
        this.madeOf = madeOf;
    }


    @Override
    public int CalculateSize() {
        return super.CalculateSize();
    }
}
